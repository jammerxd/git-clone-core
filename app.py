import time
from threading import Thread
from flask import request, Flask,abort, session,flash, redirect, url_for, render_template
import subprocess
import json
from flask_mysqldb import MySQL
from flask_bcrypt import Bcrypt
from functools import wraps
from flask_wtf.csrf import CSRFProtect
from flask_wtf.csrf import CSRFError
import sys
import os


bufsize = 8192
config = False

with open('config.json') as json_file:
	config = json.load(json_file)
	if(len(config["config"]["flask"]["route_prefix"])>0):
		if(config["config"]["flask"]["route_prefix"][0] != '/'):
			config["config"]["flask"]["route_prefix"] = "/" + config["config"]["flask"]["route_prefix"]
	


app = Flask(__name__)
app.secret_key = config["config"]["flask"]["secret"]

app.config["SESSION_COOKIE_SECURE"] = True

bcrypt=Bcrypt(app)
csrf = CSRFProtect(app)

mysql = MySQL()
# MySQL configurations
app.config['MYSQL_USER'] = config["config"]["mysql"]["user"]
app.config['MYSQL_PASSWORD'] = config["config"]["mysql"]["password"]
app.config['MYSQL_DB'] = config["config"]["mysql"]["database"]
app.config['MYSQL_HOST'] = config["config"]["mysql"]["server"]
app.config["MYSQL_PORT"] = config["config"]["mysql"]["port"]
app.config["MYSQL_CURSORCLASS"] = 'DictCursor'
app.config["MYSQL_CHARSET"] = "utf8mb4"
app.config["MYSQL_AUTOCOMMIT"] = True
mysql.init_app(app)


class Compute(Thread):
	def __init__(self, request,branch):
		Thread.__init__(self)
		self.request = request
		self.branch = branch
	def run(self):
		print("start")
		cmd = self.branch['clone_script_command']
		process = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE)
		process.wait()
		print("finished: " + str(process.returncode))

def tail_file(filename, nlines):
	with open(filename) as qfile:
		qfile.seek(0, os.SEEK_END)
		endf = position = qfile.tell()
		linecnt = 0
		while position >= 0:
			qfile.seek(position)
			next_char = qfile.read(1)
			if next_char == "\n" and position != endf-1:
				linecnt += 1

			if linecnt == nlines:
				break
			position -= 1

		if position < 0:
			qfile.seek(0)
		content = qfile.read().splitlines()
		return content

#get user by username
def get_user_by_username(username):
	return_result = False
	# Create cursor
	cur = mysql.connection.cursor()

	# Get user by username
	result = cur.execute("SELECT * FROM users WHERE username = %s LIMIT 1", [username])

	if result > 0:
		# Get stored hash
		data = cur.fetchone()
		return_result = data
	cur.close()
	return return_result


# Check if user logged in
def logged_in(f):
	@wraps(f)
	def wrap(*args, **kwargs):
		return_result = redirect(url_for('login'))
		if 'gcc_username' in session:
			if(get_user_by_username(session['gcc_username'])):
				return_result = f(*args,**kwargs)
			else:
				flash('Unauthorized, Please login', 'danger')
				session.clear()
		else:
			flash('Unauthorized, Please login', 'danger')
		return return_result
	return wrap

# Check if user not logged in
def not_logged_in(f):
	@wraps(f)
	def wrap(*args, **kwargs):
		if 'gcc_username' not in session:
			return f(*args, **kwargs)
		else:
			if(get_user_by_username(session['gcc_username']) != False):
				flash('You are already logged in.', 'danger')
				return redirect(url_for('home'))
			else:
				flash('An error has ocurred.','danger')
				return redirect(url_for('login'))
	return wrap

def reset_password_test():
    #username,password
    username = "jammerxd"
    password = "test1234"

    bcrypted = bcrypt.generate_password_hash(password)
    # Get user by username
    
    cur = mysql.connection.cursor()
    result = cur.execute("UPDATE users SET password = %s WHERE username=%s",(bcrypted,username))
    mysql.connection.commit()
    cur.close()

@app.errorhandler(CSRFError)
def handle_csrf_error(e):
    return render_template('csrf_error.html', reason=e.description), 400

		
@app.route(config["config"]["flask"]["route_prefix"]+'/doclone/', methods=["GET", "POST"])
@csrf.exempt
def doclone():
	if('X-Gitlab-Token' in request.headers):
		return_result = "Nothing"
		cur = mysql.connection.cursor()
		resultcount = cur.execute("SELECT * FROM branches,projects WHERE projects.id = branches.id_project AND gitlab_token = %s", [request.headers["X-Gitlab-Token"]])
		if(resultcount > 0):
			return_result = ""
			results = cur.fetchall()
			for i in range(len(results)):
				cur_result = results[i]
				thread_a = Compute(request.__copy__(),cur_result)
				thread_a.start()
				print("Processing clone request for branch " + cur_result["branch_name"] + " on project " + cur_result["project_name"])
				return_result += "Processing request " + str(i+1) + " ...<br>"
		cur.close()
	#	if(request.headers["X-Gitlab-Token"] == config["config"]["gitlab"]["token"]):
	#		thread_a = Compute(request.__copy__())
	#		thread_a.start()
	#		return "Processing in background", 200
	#	else:
	#		return abort(403)
	return return_result
		

@app.route(config["config"]["flask"]["route_prefix"]+'/login/', methods=["GET","POST"])
@not_logged_in
def login():

	#reset_password_test()
	#default response(GET)
	return_result = render_template('login.html', title="Login Required")
		
	if request.method == "POST":
		
		# Get Form Fields
		username = request.form['username']
		password_candidate = request.form['password']

        # Create cursor
		cur = mysql.connection.cursor()

        # Get user by username
		result = cur.execute("SELECT * FROM users WHERE username = %s LIMIT 1", [username])

		if result > 0:
        	# Get stored hash
			data = cur.fetchone()
			password = data['password']

            # Compare Passwords
			if bcrypt.check_password_hash(password, password_candidate):
				session['gcc_username'] = username
				flash('Login successful', 'success')
				return_result = redirect(url_for('home'))
			else:
				flash("Invalid login.(1)","danger")
				return_result = redirect(url_for('login'))

		else:
			flash("Invalid login.(2)","danger")
			return_result = redirect(url_for('login'))
		cur.close()
	return return_result



		
@app.route(config["config"]["flask"]["route_prefix"]+'/logout/',methods=["POST"])
@logged_in
def logout():
	if(request.method == "POST"):
		session.clear()
		flash('Logged out successfully!','success')
		return redirect(url_for('login'))
	else:
		return abort(403)

@app.route(config["config"]["flask"]["route_prefix"]+'/reset_password/',methods=["GET","POST"])
@not_logged_in
def reset_password():
	return_result = render_template('forgot_password.html',title="Forgot Password")
	if request.method == "POST":
		c = ""
	return return_result

@app.route(config["config"]["flask"]["route_prefix"]+'/change_password/',methods=["GET","POST"])
@logged_in
def change_password():
	return_result = render_template('change_password.html',title="Change Password")
	if request.method == "POST":
		return_result = redirect(url_for('change_password'))
		current_user = get_user_by_username(session['gcc_username'])
		if bcrypt.check_password_hash(current_user['password'], request.form['cur_password']):
			if(request.form['password'] == request.form['password2']):
				cur = mysql.connection.cursor()
				new_password = bcrypt.generate_password_hash(request.form['password'])
				result = cur.execute('UPDATE users SET password=%s WHERE username=%s',(new_password,current_user["username"]))
				mysql.connection.commit()
				cur.close()
				flash('Password changed successfully','success')
			else:
				flash('Passwords do not match.','danger')
		else:
			flash('Incorrect password.','danger')
	return return_result


@app.route(config["config"]["flask"]["route_prefix"]+'/manage_users/',methods=["GET","POST"])
@logged_in
def manage_users():
	return_result = render_template('manage_users.html',title="Manage Users")
	if request.method == "POST":
		pass
	return return_result


@app.route(config["config"]["flask"]["route_prefix"]+'/manage_projects/',methods=["GET","POST"])
@logged_in
def manage_projects():
	return_result = render_template('manage_projects.html',title="Manage Projects")
	if request.method == "POST":
		if('form' in request.form):
			return_result = redirect(url_for('manage_projects'))
			if(request.form['form'] == 'form_new_project'):
				new_project_name = request.form['new_project_name']
				new_project_clone_url = request.form['new_project_url']

				cur = mysql.connection.cursor()
				result = cur.execute('SELECT * FROM projects WHERE project_name = %s or clone_url = %s LIMIT 1',(new_project_name,new_project_clone_url))
				if result > 0:
					flash('Project name or clone URL already exists','danger')
				else:
					cur.nextset()
					next_result = cur.execute('INSERT INTO projects (project_name, clone_url) VALUES (%s,%s)',(new_project_name,new_project_clone_url))
					mysql.connection.commit()
					cur.close()
					if next_result > 0:
						flash('New project added successfully','success')
					else:
						flash('Failed to add new project','danger')
				cur.close()
			elif (request.form['form'] == 'form-delete-project'):
				project_to_delete = request.form['project_to_delete_id']
				cur = mysql.connection.cursor()
				delete = cur.execute('DELETE FROM branches WHERE id_project = %s',(project_to_delete))
				mysql.connection.commit()
				cur.nextset()
				delete2 = cur.execute('DELETE FROM projects WHERE id = %s',(project_to_delete))
				mysql.connection.commit()
				cur.close()
				flash('Deleted project','success')
				
	elif request.method == "GET":
		cur = mysql.connection.cursor()
		resultcount = cur.execute("SELECT * FROM projects")
		results = cur.fetchall()
		cur.close()
		return_result = render_template('manage_projects.html',title="Manage Projects",project_list=results)				
	return return_result


# @app.route(config["config"]["flask"]["route_prefix"]+'/manage_branches/',methods=["GET","POST"])
# @logged_in
# def manage_branches():
# 	return_result = render_template('manage_branches.html',title="Manage Branches")
# 	if request.method == "POST":
# 		if('form' in request.form):
# 			return_result = redirect(url_for('manage_branches'))
# 			if(request.form['form'] == 'form_new_branch'):
# 				new_branch_project_id = request.form['new_branch_project_id']
# 				new_branch_name = request.form['new_branch_name']
# 				new_branch_script_path = request.form['new_branch_script_path']
# 				new_branch_script_command = request.form['new_branch_script_command']
# 				new_branch_gitlab_token = request.form['new_branch_gitlab_token']
# 				new_branch_pm2_id = request.form['new_branch_pm2_id']
# 				cur = mysql.connection.cursor()
# 				result = cur.execute('SELECT * FROM branches WHERE id_project = %s AND ((branch_name = %s OR gitlab_token = %s) OR (branch_name = %s AND gitlab_token = %s)) LIMIT 1',(new_branch_project_id,new_branch_name,new_branch_gitlab_token,new_branch_name,new_branch_gitlab_token))
# 				if result > 0:
# 					flash('Branch name, Project, Gitlab Token combination already exist.','danger')
# 				else:
# 					cur.nextset()
# 					next_result = cur.execute('INSERT INTO branches (id_project, branch_name,clone_script_path,clone_script_command,gitlab_token,pm2_id) VALUES (%s,%s,%s,%s,%s,%s)',(new_branch_project_id,new_branch_name,new_branch_script_path,new_branch_script_command,new_branch_gitlab_token,new_branch_pm2_id))
# 					mysql.connection.commit()
# 					if next_result > 0:
# 						flash('New branch added successfully','success')
# 					else:
# 						flash('Failed to add new branch','danger')
# 				cur.close()
# 			elif (request.form['form'] == 'form-delete-branch'):
# 				branch_to_delete = request.form['branch_to_delete_id']
# 				cur = mysql.connection.cursor()
# 				delete = cur.execute('DELETE FROM branches WHERE id = %s',(branch_to_delete))
# 				mysql.connection.commit()
# 				cur.close()
# 				flash('Deleted branch','success')
# 	elif request.method == "GET":
# 		cur = mysql.connection.cursor()
# 		resultcount = cur.execute("SELECT * FROM projects")
# 		project_list = cur.fetchall()
# 		cur.nextset()
# 		resultcount2 = cur.execute("SELECT * FROM branches,projects WHERE branches.id_project = projects.id")
# 		branch_list = cur.fetchall()
# 		cur.close()
# 		return_result = render_template('manage_branches.html',title="Manage Branches",project_list=project_list,branch_list=branch_list)
# 	return return_result

@app.route(config["config"]["flask"]["route_prefix"]+'/manage_projets/<project_id>/branches/',methods=["GET","POST"])
@logged_in
def manage_branches_single_project(project_id):
	return_result = render_template('manage_branches_single_project.html',title="Manage Branches")
	if request.method == "POST":
		if('form' in request.form):
			return_result = redirect(url_for('manage_branches_single_project',project_id=project_id))
			if(request.form['form'] == 'form_new_branch'):
				
				new_branch_name = request.form['new_branch_name']
				new_branch_script_path = request.form['new_branch_script_path']
				new_branch_script_command = request.form['new_branch_script_command']
				new_branch_gitlab_token = request.form['new_branch_gitlab_token']
				new_branch_pm2_id = request.form['new_branch_pm2_id']
				new_branch_normal_log_path = request.form['new_branch_log_path']
				new_branch_error_log_path = request.form['new_branch_error_log_path']
				cur = mysql.connection.cursor()
				
				
				result = cur.execute('SELECT * FROM branches WHERE id_project = %s AND ((branch_name = %s OR gitlab_token = %s) OR (branch_name = %s AND gitlab_token = %s)) LIMIT 1',(project_id,new_branch_name,new_branch_gitlab_token,new_branch_name,new_branch_gitlab_token))
				if result > 0:
					flash('Branch name, Project, Gitlab Token combination already exist.','danger')
				else:

					pm2_id_sql = 'NULL'
					normal_log_path_sql = 'NULL'
					error_log_path_sql = 'NULL'
					cur.nextset()
					if(len(new_branch_pm2_id) > 0):	
						pm2_id_sql = new_branch_pm2_id
					if(len(new_branch_error_log_path) > 0):
						error_log_path_sql = '"' + new_branch_error_log_path + '"'
					if(len(new_branch_normal_log_path) > 0):
						normal_log_path_sql = '"' + new_branch_normal_log_path + '"'
					next_result = cur.execute('INSERT INTO branches (id_project, branch_name,clone_script_path,clone_script_command,gitlab_token,normal_log_path,error_log_path,pm2_id) VALUES (%s,%s,%s,%s,%s,' + normal_log_path_sql + ',' + error_log_path_sql + ',' + pm2_id_sql + ')',(project_id,new_branch_name,new_branch_script_path,new_branch_script_command,new_branch_gitlab_token))
					mysql.connection.commit()
					if next_result > 0:
						flash('New branch added successfully','success')
					else:
						flash('Failed to add new branch','danger')
				cur.close()
			elif (request.form['form'] == 'form-delete-branch'):
				branch_to_delete = request.form['branch_to_delete_id']
				cur = mysql.connection.cursor()
				delete = cur.execute('DELETE FROM branches WHERE id = %s',[branch_to_delete])
				mysql.connection.commit()
				cur.close()
				flash('Deleted branch','success')
	elif request.method == "GET":
		cur = mysql.connection.cursor()
		resultcount2 = cur.execute("SELECT * FROM branches,projects WHERE branches.id_project = projects.id AND branches.id_project = %s",[project_id])
		branch_list = cur.fetchall()
		cur.close()
		return_result = render_template('manage_branches_single_project.html',title="Manage Branches",project_id=project_id,branch_list=branch_list)
	return return_result

@app.route(config["config"]["flask"]["route_prefix"]+'/manage_projets/<project_id>/branches/<branch_id>/logs',methods=["GET","POST"])
@logged_in
def branch_logs(project_id,branch_id):
	return_result = render_template('branch_logs.html',title="Branch Logs",project_id=project_id,branch_id=branch_id)
	if(branch_id >= 0):
		cur = mysql.connection.cursor()
		resultcount = cur.execute("SELECT * FROM branches WHERE id = %s LIMIT 1", [branch_id])
		

		if(resultcount > 0):
			branch_result = cur.fetchone()
			pm2_id = branch_result['pm2_id']
			norm_log_path = branch_result['normal_log_path']
			error_log_path = branch_result['error_log_path']
			err_log_data = json.loads("{}")
			if(not error_log_path is None and error_log_path != ''):
				if(os.path.isfile(error_log_path)):
					err_log_data_raw = tail_file(error_log_path,config["config"]["pm2"]["line_count"])
					if(len(err_log_data_raw)>0):
						err_log_data = []
						for i in range(0,len(err_log_data_raw)):
							line = err_log_data_raw[i].decode('utf-8').strip()
							line = line[line.find("{"):line.rfind("}")+1]
							if(line.find("{") > -1 and line.rfind("}") > 0):
								line_json = json.loads(line)
								err_log_data.append({"message":line_json["message"],
								"timestamp":line_json["timestamp"],
								"app_name":line_json["app_name"],
								"process_id": line_json["process_id"],
								"type":line_json["type"]
								})
				

			norm_log_data = json.loads("{}")
			if(not norm_log_path is None and norm_log_path != ''):
				if(os.path.isfile(norm_log_path) ):
					norm_log_data_raw = tail_file(norm_log_path,config["config"]["pm2"]["line_count"])
					if(len(norm_log_data_raw)>0):
						norm_log_data = []
						for i in range(0,len(norm_log_data_raw)):
							line = norm_log_data_raw[i].decode('utf-8').strip()
							line = line[line.find("{"):line.rfind("}")+1]
							if(line.find("{") > -1 and line.rfind("}")+1 > 0):
								line_json = json.loads(line)
								norm_log_data.append({"message":line_json["message"],
								"timestamp":line_json["timestamp"],
								"app_name":line_json["app_name"],
								"process_id": line_json["process_id"],
								"type":line_json["type"]
								})


			

			return_result = render_template('branch_logs.html',title="Branch Logs",err_log_data=err_log_data,norm_log_data=norm_log_data,project_id=project_id,branch_id=branch_id)
		else:
			flash('That pm2 id does not exist.','danger')
			return_result = redirect(url_for('manage_branches_single_project',project_id=project_id))
		cur.close()
	else:
		flash('PM2 id must be 0 or higher.','danger')
		return_result = redirect(url_for('manage_branches_single_project',project_id=project_id))
	

	return return_result


@app.route(config["config"]["flask"]["route_prefix"]+'/',methods=["GET"])
@logged_in
def home():
	return render_template('home.html', title="Git Clone Core")
if __name__ == "__main__":
	app.run(debug=True,port=8443,ssl_context=(config["config"]["flask"]["cert"],config["config"]["flask"]["cert_private"]))
